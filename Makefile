DEVICE := sfo

MODEM_IMAGE := firmware-update/NON-HLOS.bin
SBL_ELF := firmware-update/sbl1.mbn
APPSBOOT_MBN := firmware-update/emmc_appsboot.mbn

VERSION := $(shell strings $(MODEM_IMAGE) | sed -n 's|QC_IMAGE_VERSION_STRING\=MPSS\.DI\.\(.*\)|\1|p')

HASH_SBL := $(shell openssl dgst -r -sha1 $(SBL_ELF) | cut -d ' ' -f 1)
HASH_APPSBOOT := $(shell openssl dgst -r -sha1 $(APPSBOOT_MBN) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(SBL_ELF) $(APPSBOOT_MBN)
ifneq ($(HASH_SBL), 3187d40e2797be0321dda456aec2388fb1b27c28)
	$(error SHA-1 of sbl1.elf mismatch)
endif
ifneq ($(HASH_APPSBOOT), 3cc9bae74a945ca42d425eb055cdd65309daa33e)
	$(error SHA-1 of emmc_appsboot.mbn mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect:
	@echo Target: $(TARGET)
	@echo Version: $(VERSION)
